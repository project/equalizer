<div class="comment <?php print ($comment->new) ? 'comment-new' : '' ?>"> 
  <p class="header">
    <?php if ($comment->new) { ?> 
      <span class="new">
	    <?php print $new ?>
	  </span>
    <?php }; ?>
    <div class="comment-title">
      <?php print $title ?>
    </div>
    <div class="comment-author">
      <?php if ($picture) { ?>
        <?php print $picture ?>
      <?php }; ?>
      <?php print $author ?> | <?php print $date ?>
    </div>
  </p>   
  <div class="content"><?php print $content ?></div> 
  <div class="postmetadata">
    <?php print $links ?>
  </div> 
</div>